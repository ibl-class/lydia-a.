package ke.ac.lady.lydia.itutor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button TUTOR;
    private Button STUDENT;
    private Button PARENT;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button TUTOR = (Button) findViewById(R.id.tutorBT);
        TUTOR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), T_Register.class);
                startActivity(intent);
            }
        });

        Button STUDENT = (Button) findViewById(R.id.studentBT);
        STUDENT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),S_Register.class);
                startActivity(intent);
            }
        });

        Button PARENT = (Button) findViewById(R.id.parentBT);
        PARENT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),P_Register.class);
                startActivity(intent);
            }
        });
    }
}