package ke.ac.lady.lydia.itutor;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class P_Menu extends AppCompatActivity {
    private Button report;
    private ListView lreport;
    List<Report> reports;
    DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p__menu);

        mDatabase = FirebaseDatabase.getInstance().getReference("Reports");
        
        report = (Button) findViewById(R.id.reports);
        lreport = (ListView) findViewById(R.id.reportsLV);
        
        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getReports();
            }
        });
    }

    private void getReports() {
        mDatabase.orderByChild("RegNumber").equalTo("Reg").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //clearing the previous report list
                reports.clear();

                //iterating through all the nodes
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    //getting report
                    Report report = postSnapshot.getValue(Report.class);
                    //adding report to the list
                    reports.add(report);
                }

                //creating adapter
                Reports reportList = new Reports(P_Menu.this, reports);
                //attaching adapter to the listview
                lreport.setAdapter(reportList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(),"Could not retrieve reports",Toast.LENGTH_LONG).show();
            }
        });

    }
}