package ke.ac.lady.lydia.itutor;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class S_Menu extends AppCompatActivity {
    private Button schedule;
    private ListView lschedule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s__menu);

        schedule = (Button)findViewById(R.id.schedule);
        lschedule = (ListView)findViewById(R.id.scheduleLV);

        schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSchedule();
            }
        });
    }

    private void getSchedule() {

    }
}