package ke.ac.lady.lydia.itutor;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class AddReportDialog extends Dialog {


    public AddReportDialog(@NonNull Context context) {
        super(context);
    }

    public AddReportDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected AddReportDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }
}
