package ke.ac.lady.lydia.itutor;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class ViewItem extends AppCompatActivity implements View.OnClickListener {

    private DatabaseReference mRef;
    private FirebaseDatabase mDatabase;

    public Context c=ViewItem.this;
    private Button btnReport;
    private TextView txtFname,txtLName,txtRegNo;
    private String studFName,studLName,studReg;
    private EditText txtReport;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.single_student_view);
        getWindow().setStatusBarColor(Color.BLACK);

        setUpWidgets();
        getActivityData();

        mDatabase=FirebaseDatabase.getInstance();
        mRef=mDatabase.getReference();
        mRef.child("Students").orderByChild("RegNumber").equalTo(studReg).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    for (DataSnapshot snapshot1:snapshot.getChildren()){
                        Students students=snapshot1.getValue(Students.class);
                        studFName=students.getFirstName();
                        studLName=students.getLastName();
                        studReg=students.getRegNumber();

                    }
                }else{
                    Toast.makeText(c,"Could not get item Details",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void getActivityData() {
        Bundle extras=getIntent().getExtras();
        if (extras!=null){
            studReg=getIntent().getStringExtra("RegNumber");
        }else{
            Toast.makeText(c,"Could not get Item Details",Toast.LENGTH_SHORT).show();
        }
    }

    private void setUpWidgets() {
        txtFname=findViewById(R.id.fname);
        txtLName=findViewById(R.id.lname);
        txtRegNo=findViewById(R.id.regno);
        btnReport=findViewById(R.id.addReport);
        txtReport=findViewById(R.id.editEnterReport);

    }


    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onStart() {
        super.onStart();
    }
}