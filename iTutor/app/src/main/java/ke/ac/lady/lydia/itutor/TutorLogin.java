package ke.ac.lady.lydia.itutor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class TutorLogin extends AppCompatActivity {
    private EditText email, password;
    private Button signIn;
    private TextView signup;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        auth = FirebaseAuth.getInstance();

        if (auth.getCurrentUser() != null) {
            startActivity(new Intent(TutorLogin.this, TutorMenu.class));
            finish();
        }

        setContentView(R.layout.activity_tutor_login);

        email = (EditText) findViewById(R.id.email1ET);
        password = (EditText) findViewById(R.id.pass1ET);
        signIn = (Button) findViewById(R.id.loginBT);
        signup=(TextView) findViewById(R.id.signupTV);

        auth = FirebaseAuth.getInstance();

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), T_Register.class);
                startActivity(intent);
                finish();
            }
        });

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String e = email.getText().toString();
                String p = password.getText().toString();

                if (TextUtils.isEmpty(email.getText()) || TextUtils.isEmpty(password.getText())) {
                    Toast.makeText(TutorLogin.this, "Please fill in all the fields", Toast.LENGTH_SHORT).show();
                } else if (!p.equals("123456") && !e.equals("leeoketch@gmail.com")) {
                    Toast.makeText(TutorLogin.this, "Invalid email/ password", Toast.LENGTH_SHORT).show();

                } else {
                    auth.signInWithEmailAndPassword(e, p)
                            .addOnCompleteListener(TutorLogin.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {

                                    if (!task.isSuccessful()) {
                                        Toast.makeText(TutorLogin.this, "Authentication failed." + task.getException(),
                                                Toast.LENGTH_LONG).show();
                                    }else{

                                        Intent intent = new Intent(getApplicationContext(), TutorMenu.class);
                                        startActivity(intent);
                                        finish();}
                                }
                            });
                }
            }

        });
    }}