package ke.ac.lady.lydia.itutor;

public class Students {
    private String regNumber, FirstName, LastName;

    public Students(String regNumber, String firstName, String lastName) {
        this.regNumber = regNumber;
        FirstName = firstName;
        LastName = lastName;
    }

    public Students() {
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }
}
