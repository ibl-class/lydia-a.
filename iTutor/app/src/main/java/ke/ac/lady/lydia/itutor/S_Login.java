package ke.ac.lady.lydia.itutor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class S_Login extends AppCompatActivity {
    private static final String TAG = "S_Login";
    private EditText reg;
    private Button login;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s__login);

        mDatabase = FirebaseDatabase.getInstance().getReference("Students");

        reg = (EditText) findViewById(R.id.regnET);
        login = (Button) findViewById(R.id.loginBT);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(reg.getText())){
                    Toast.makeText(S_Login.this,"Please fill in all the fields",Toast.LENGTH_SHORT).show();

                }else {
                    String RegN =reg.getText().toString();

                    mDatabase.orderByChild("RegNo").equalTo(RegN).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            if (dataSnapshot.exists()) {
                                Log.d(TAG, "DataSnapshot data: " + dataSnapshot.getValue());

                                Intent intent = new Intent(getApplicationContext(),S_Menu.class);
                                startActivity(intent);
                                finish();

                            } else {
                                Log.d(TAG, "Registration Number does not exist");
                                Toast.makeText(S_Login.this,"Registration Number does not exist",Toast.LENGTH_LONG).show();
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {
                            Toast.makeText(getApplicationContext(),"Could not read data",Toast.LENGTH_LONG).show();
                        }
                    });


                }
            }
        });
    }
}
