package ke.ac.lady.lydia.itutor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class TutorMenu extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG="TutorMenu";
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private NavigationView navigationView;
    private LinearLayoutManager linearLayoutManager;

    private DatabaseReference mDatabaseReference;
    //private FirebaseRecyclerAdapter<ItemDets, PostViewHolder> mAdapter;
    private FirebaseAuth mFirebaseAuth;

    private ProgressBar progressBar;
    private Dialog mDialog;
    private TextView txtLoading;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private StudentAdapter studentAdapter;
    private ArrayList<Students> students;
    private Context mContext=TutorMenu.this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutor_menu);

        mFirebaseAuth= FirebaseAuth.getInstance();

        if (mFirebaseAuth.getCurrentUser()==null){
            startActivity(new Intent(getApplicationContext(),TutorLogin.class));
            finish();
        }
        Log.d(TAG,"No user Logged in.");


        mFirebaseAuth=FirebaseAuth.getInstance();

        mDatabaseReference= FirebaseDatabase.getInstance().getReference();

        //setup Widgets
        mDrawerLayout=findViewById(R.id.drawer);
        navigationView=findViewById(R.id.nav_view);
        progressBar=findViewById(R.id.progressBarHome);
        txtLoading=findViewById(R.id.stdLoading);

        navigationView.setNavigationItemSelectedListener(this);

        mDrawerToggle=new ActionBarDrawerToggle(this,mDrawerLayout,R.string.open,R.string.close);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView=(RecyclerView)findViewById(R.id.recycler_view_home);
        linearLayoutManager=new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(mAdapter);
        students = new ArrayList<Students>();


        mDatabaseReference.child("Students").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    progressBar.setVisibility(View.INVISIBLE);
                    txtLoading.setVisibility(View.INVISIBLE);

                    for (DataSnapshot ds1 : snapshot.getChildren()) {
                        try {
                            Students student = ds1.getValue(Students.class);
                            students.add(student);
                            progressBar.setVisibility(View.INVISIBLE);
                            txtLoading.setVisibility(View.INVISIBLE);

                        } catch (Exception e) {
                            Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }

                    studentAdapter = new StudentAdapter(students, TutorMenu.this);
                    recyclerView.setAdapter(studentAdapter);

                } else {
                    Toast.makeText(getApplicationContext(), "No Students Yet", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.INVISIBLE);
                    txtLoading.setText(R.string.no_student_added);
                }
            }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(mContext,"Error connecting to Database",Toast.LENGTH_SHORT).show();

                }
            });

        }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if (mDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item){
        int id=item.getItemId();

        if (id==R.id.add){
            startActivity(new Intent(mContext,AddDelActivity.class));
        }else if (id==R.id.uploadtt) {
            startActivity(new Intent(mContext, Schedule.class));
        }else  if(id==R.id.logout){
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(mContext, TutorLogin.class));
            finish();
        }

        DrawerLayout drawerLayout=findViewById(R.id.drawer);
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent a=new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
        //super.onBackPressed();
    }

    public static class StudentsViewHolder extends RecyclerView.ViewHolder{
        TextView RegNumber,FirstName,LastName;
        Button reportBT;
        boolean addedReport;
        String report;

        public StudentsViewHolder(@NonNull View itemView) {
            super(itemView);
            RegNumber = itemView.findViewById(R.id.regno);
            FirstName = itemView.findViewById(R.id.fname);
            LastName = itemView.findViewById(R.id.lname);
            reportBT = itemView.findViewById(R.id.addReport);
            addedReport=false;
            report="";
        }
    }
    private String getDateTime() {

        DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);

    }


}