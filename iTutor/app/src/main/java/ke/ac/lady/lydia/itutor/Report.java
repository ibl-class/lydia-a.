package ke.ac.lady.lydia.itutor;

public class Report {
    private String Date, Report;

    public Report(String date, String report) {
        Date = date;
        Report = report;
    }

    public Report() {
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getReport() {
        return Report;
    }

    public void setReport(String report) {
        Report = report;
    }
}
