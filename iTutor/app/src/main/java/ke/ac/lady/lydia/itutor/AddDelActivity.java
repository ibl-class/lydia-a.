package ke.ac.lady.lydia.itutor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class AddDelActivity extends AppCompatActivity {

    private static final String TAG = "AddDelActivity";
    private EditText regNo,subject;
    private Button add,del;
    FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_del);
        db = FirebaseFirestore.getInstance();

        regNo = (EditText) findViewById(R.id.regET);
        subject = (EditText) findViewById(R.id.subjectET);
        add = (Button) findViewById(R.id.addBT);
        del = (Button) findViewById(R.id.delBT);



        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(subject.getText())|| TextUtils.isEmpty(regNo.getText())){
                    Toast.makeText(AddDelActivity.this,"Please fill in all the fields",Toast.LENGTH_SHORT).show();

                }else {
                    String RegN =regNo.getText().toString();
                    String Subject = subject.getText().toString();

// Create a new student
                    Map<String, Object> student = new HashMap<>();
                    student.put("RegNumber", RegN);
                    student.put("Subject", Subject);

// Add a new document with a generated ID
                    db.collection("students").document(RegN)
                            .set(student)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.d(TAG, "DocumentSnapshot successfully written!");
                                    Toast.makeText(AddDelActivity.this,"student successfully added",Toast.LENGTH_LONG).show();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w(TAG, "Error writing document", e);
                                    Toast.makeText(AddDelActivity.this,"Error writing document",Toast.LENGTH_LONG).show();
                                }
                            });


                }}
        });
        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                db.collection("students").document(String.valueOf(regNo))
                        .delete()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d(TAG, "Student successfully deleted!");
                                Toast.makeText(AddDelActivity.this,"Student successfully deleted",Toast.LENGTH_LONG).show();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w(TAG, "Error deleting document", e);
                            }
                        });
            }
        });

    }
}