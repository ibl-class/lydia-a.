package ke.ac.lady.lydia.itutor;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.MyViewHolder> {

    private String [] mDataset;
    private Dialog mDialog;
    private ArrayList<Students> students;
    private Context mContext;
    private Button btnEnterReport;
    private DatabaseReference mDatabase;

    public StudentAdapter(ArrayList<Students> students, Context context) {
        this.students = students;
        this.mContext = context;
    }

    public StudentAdapter(String [] myDataset){
        this.mDataset=myDataset;
    }

    @NonNull
    @Override
    public StudentAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.single_student,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull StudentAdapter.MyViewHolder holder, int position) {
        final String RegNumber = students.get(position).getRegNumber();
        final String FirstName = students.get(position).getFirstName();
        final String LastName = students.get(position).getLastName();


        Toast.makeText(mContext,FirstName, Toast.LENGTH_SHORT).show();

        holder.RegNumber.setText(RegNumber);
        holder.FirstName.setText(FirstName);
        holder.LastName.setText(LastName);


        holder.reportBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!holder.addedReport){
                    createDialog(holder);
                    Toast.makeText(mContext,"Add a report Button",Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(mContext,"Added report is: "+holder.report,Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void createDialog(MyViewHolder holder) {
        mDialog=new Dialog(mContext);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.add_report);
        mDialog.setTitle("Report");

        btnEnterReport=mDialog.findViewById(R.id.btnreportDiag);
        final EditText editReport=mDialog.findViewById(R.id.editEnterReport);

        btnEnterReport.setEnabled(true);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        btnEnterReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!editReport.getText().toString().isEmpty()){
                    holder.report=editReport.getText().toString().trim();
                    //holder.RegNumber = regNo.getText().toString().trim();
                    holder.addedReport=true;
                    //Saving the Student
                    //mDatabase.child("Reports").child(RegNo).setValue(students);

                    Toast.makeText(mContext,"Added report is: "+holder.report,Toast.LENGTH_SHORT).show();
                    mDialog.dismiss();
                }
            }
        });

        mDialog.show();

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext,ViewItem.class);
                //intent.putExtra("postId",Regno);
                mContext.startActivity(intent);

            }
        });
    }




    @Override
    public int getItemCount() {
        return students.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout view;
        TextView RegNumber, FirstName, LastName;
        Button reportBT;
        boolean addedReport;
        String report;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            view = (LinearLayout) itemView.findViewById(R.id.view);
            RegNumber = (TextView) itemView.findViewById(R.id.regno);
            FirstName = (TextView) itemView.findViewById(R.id.fname);
            LastName = (TextView) itemView.findViewById(R.id.lname);
            reportBT = (Button) itemView.findViewById(R.id.addReport);
            addedReport = false;
            report = "";
        }


    }
}
