package ke.ac.lady.lydia.itutor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class S_SignUp extends AppCompatActivity {

    private EditText fname;
    private EditText lname;
    private EditText regNo;
    private Button signUp;

    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s__sign_up);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        fname = (EditText) findViewById(R.id.fNameET);
        lname = (EditText) findViewById(R.id.lNameET);
        regNo = (EditText) findViewById(R.id.regET);
        signUp = (Button) findViewById(R.id.signupBT);

       signUp.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               addStudent();
           }
       });

    }

    private void addStudent() {
        //getting the values to save
        String FirstName = fname.getText().toString().trim();
        String LastName = lname.getText().toString().trim();
        String RegNo = regNo.getText().toString().trim();

        //checking if the value is provided
        if (!TextUtils.isEmpty(fname.getText().toString())  ||!TextUtils.isEmpty(lname.getText().toString()) || !TextUtils.isEmpty(regNo.getText().toString())) {

            //creating a Students Object
            Students students = new Students(FirstName,LastName,RegNo);

            //Saving the Student
            mDatabase.child("Students").child(RegNo).setValue(students);

            //displaying a success toast
            Toast.makeText(this, "Student added", Toast.LENGTH_LONG).show();

            Intent intent = new Intent(getApplicationContext(),S_Menu.class);
            startActivity(intent);
            finish();
        } else {
            //if the value is not given displaying a toast
            Toast.makeText(this, "Please fill all the fields", Toast.LENGTH_LONG).show();
        }
    }

}