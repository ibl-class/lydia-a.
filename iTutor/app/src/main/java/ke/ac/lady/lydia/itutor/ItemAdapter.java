package ke.ac.lady.lydia.itutor;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<Students> students;

    public ItemAdapter(Context context,ArrayList<Students> student){
        this.mContext=context;
        this.students=student;

    }


    @NonNull
    @Override
    public ItemAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.single_student,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemAdapter.MyViewHolder holder, int position) {


    }

    @Override
    public int getItemCount() {
        return 0;
    }
    class MyViewHolder extends RecyclerView.ViewHolder{

        LinearLayout view;
        Button btnReport;
        TextView Fname,Lname,RegNo;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            view=itemView.findViewById(R.id.view1);
            Fname=itemView.findViewById(R.id.regno);
            Lname=itemView.findViewById(R.id.fname);
            RegNo=itemView.findViewById(R.id.lname);
            btnReport=itemView.findViewById(R.id.addReport);
        }
    }
}
