package ke.ac.lady.lydia.itutor;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

public class Reports extends ArrayAdapter<Report>{

    private Activity context;
    List<Report> reports;

    public Reports(Activity context, List<Report> reports) {
        super(context, R.layout.custom_list_item, reports);
        this.context = context;
        this.reports = reports;
    }

    public Reports(@NonNull Context context, int resource) {
        super(context, resource);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.custom_list_item, null, true);

        TextView date = (TextView) listViewItem.findViewById(R.id.date);
        TextView rpo = (TextView) listViewItem.findViewById(R.id.rpo);


        Report report = reports.get(position);
        date.setText(report.getDate());
        rpo.setText(report.getReport());

        return listViewItem;
    }


}
