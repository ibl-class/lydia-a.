package ke.ac.lady.lydia.itutor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


public class T_Register extends AppCompatActivity {
    private EditText email, password;
    private Button register;
    private FirebaseAuth auth;
    private TextView login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t__register);

        auth = FirebaseAuth.getInstance();

        email = (EditText) findViewById(R.id.emailET);
        password = (EditText) findViewById(R.id.passET);
        login = (TextView) findViewById(R.id.tloginTV);
        register = (Button) findViewById(R.id.registerBT);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), TutorLogin.class);
                startActivity(intent);
            }
        });


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String e = email.getText().toString();
                String p = password.getText().toString();

                if (TextUtils.isEmpty(email.getText()) || TextUtils.isEmpty(password.getText())) {
                    Toast.makeText(T_Register.this, "Please fill in all the fields", Toast.LENGTH_SHORT).show();
                } else if (!p.equals("123456") && !e.equals("leeoketch@gmail.com")) {
                    Toast.makeText(T_Register.this, "Invalid email/ password", Toast.LENGTH_SHORT).show();

                } else {
                    auth.createUserWithEmailAndPassword(e, p)
                            .addOnCompleteListener(T_Register.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    Toast.makeText(T_Register.this, "Email and Password Correct" + task.isSuccessful(), Toast.LENGTH_LONG).show();

                                    if (!task.isSuccessful()) {
                                        Toast.makeText(T_Register.this, "Authentication failed." + task.getException(),
                                                Toast.LENGTH_LONG).show();
                                    }else{

                                        Intent intent = new Intent(getApplicationContext(), TutorMenu.class);
                                        startActivity(intent);
                                        finish();}
                                }
                            });
                }
            }

        });



    }

}